require('newrelic');
require('dotenv').config();

let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const passport = require("./config/passport");
const MongoDBStore = require("connect-mongodb-session")(session);
const session = require("express-session");
const jwt = require("jsonwebtoken");

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let tokenRouter = require('./routes/token');
let usuariosRouter = require('./routes/usuarios');
let bicicletasRouter = require('./routes/bicicletas');
let bicicletasAPIRouter = require('./routes/api/bicicletas');
let usuariosAPIRouter = require('./routes/api/usuarios')
let authAPIRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require('./models/token');

let app = express();

app.set("secretKey", "jwt_pwd_!!223344");

let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });
  store.on("error", function (err) {
    assert.ifError(err);
    assert.ok(false);
  });
}

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: "true",
  secret: 'red_bicis_!!!***!".!.!.123123',
}));

let mongoose = require('mongoose')


//let mongoDB = process.env.MONGO_URI;
let mongoDB = 'mongodb+srv://admin:admin123@red-bicicletas.md0o4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
mongoose.connect(mongoDB,{ useNewUrlParser: true});
mongoose.promise = global.promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongoDB connection error:'));
mongoose.Promise = global.Promise;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/login', function(req, res) {
  res.render('session/login');
});

app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, usuario, info){
    if (err) {return next(err);}
    if (!usuario) { return res.render('session/login', { info });}
    req.logIn(usuario, function (err) {if (err) {return next(err);}
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function (req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res, next) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) {
      return res.render('session/forgotPassword', {
        info: { message: 'No existe el email para el usuario existente' }
      });
    }

    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage')
  });
});

app.get('/resetPassword/:token', function(req, res, next) {
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token) {
      return res.status(400).send({
        type: 'not-verified',
        msg: 'No existe usuario asociado al token. Verifique que su token no haya expirado'
      });
    }

    Usuario.findById(token.usuario, function (err, usuario) {
      if (!usuario) {
        return res.status(400).send({
          msg: 'No existe un usuario asociado al token'
        });
      }

      res.render('session/resetPassword', { errors: {}, usuario:usuario });
    });
  });
});

app.use('/privacy_policy', function(req, res){
  res.sendFile('public/policy_privacy.html');
});

app.use('/googlefb2ad30e42341a79', function(req, res){
  res.sendFile('public/googlefb2ad30e42341a79.html');
});

app.post('/resetPassword', function(req, res){
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {
      errors: {
        confirm_password: {
          message: 'No coincide con el password ingresado'
        }
      },
      usuario: new Usuario({ email: req.body.email })
    });
    return;
  }

  app.get(
      "/auth/google",
      passport.authenticate("google", {
        scope: [
          "https://www.googleapis.com/auth/plus.login",
          "https://www.googleapis.com/auth/plus.profile.emails.read",
          "profile",
          "email",
        ],
      })
  );

  app.get(
      "/auth/google/callback",
      passport.authenticate("google", {
        successRedirect: "/",
        failureRedirect: "/error",
      })
  );

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;

    usuario.save(function(err){
      if (err) {
        res.render('session/resetPassword', {
          errors: err.errors,
          usuario: new Usuario({ email: req.body.email })
        });
      }else{
        res.redirect('/login');
      }
    });
  });
});

//rutas
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter);
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/api/auth', authAPIRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Usuario sin loguearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({ status: 'error', message: err.message, data: null });
    } else {

      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);

      next();
    }
  });
}

module.exports = app;
