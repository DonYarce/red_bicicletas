var map = L.map('main_map').setView([6.361629039883739, -75.50628026892643], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution:'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map)

$.ajax({
    dataType:'json',
    url:'api/bicicletas',
    success: function(result){
        console.log(result)
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        })
    }
})


//L.marker([6.34512272338332, -75.50997098823117]).addTo(map);
//L.marker([6.346103728885741, -75.5081256285788]).addTo(map);
//L.marker([6.344760177398851, -75.51724513848875]).addTo(map);